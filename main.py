#!/usr/bin/python3
import configparser
import os
import signal

import coloredlogs
import logging
from lib.bluetoothServer import BluetoothServer
import lib.successfulDataHandler as DataHandler
# Setup Logging
logger = logging.getLogger(__name__)
coloredlogs.install(level="DEBUG", logger=logger)

# Check if started with SUDO
if os.geteuid() != 0:
    logger.critical("Please run with sudo privileges. These are needed to start up the Bluetooth Server")
    exit(1)

# Config Filename
configFileName = os.path.join(os.path.dirname(__file__), "config.cfg")
config = configparser.ConfigParser()
with open(configFileName, "r") as conf:
    config.read_file(conf)

cfg = config["DEFAULT"]


# Register SIGINT Handler
def handleSigInt(sig, frame):
    logger.info("Received SIGINT Signal. Shutting down Server gracefully...")
    server.kill()


signal.signal(signal.SIGINT, handleSigInt)  # Handle Ctrl+C
signal.signal(signal.SIGTERM, handleSigInt)  # Handle Shutdown
server = BluetoothServer(int(cfg["rxBufferSize"]))
server.observer.subscribe(DataHandler.handleInboundCleartext)
logger.info("Created Server successfully.")
server.start()
