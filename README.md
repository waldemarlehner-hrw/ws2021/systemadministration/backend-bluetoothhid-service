# Backend BluetoothHID Service

This program is part of project for setting up a Raspberry Pi to transmit Passwords via Bluetooth.

This repository contains the Server-Side of this Project. 
Please note that you need to download and build **sendHID** on the Pi.

You can download sendHID from https://github.com/girst/sendHID-mirror-of-git.gir.st.git .
