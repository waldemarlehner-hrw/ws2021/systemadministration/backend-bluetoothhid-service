from socket import socket as Socket
import threading
import logging
import coloredlogs
from bluetooth import BluetoothError

from lib.bluetoothServer import BluetoothServer


class BluetoothClient:
    """
    This handles a direct connection with a client.
    'Under the hood' this uses a (blocking) receiver Thread hooked up to a Observer Pattern.
    """
    def __init__(self, parent: BluetoothServer, sock: Socket, info: any, bufferSize: int):
        self.__logger = logging.getLogger(self.__class__.__name__)
        coloredlogs.install(level="DEBUG", logger=self.__logger)

        self.__logger.info("Creating new Client for "+str(info))
        self.__bufferSize = bufferSize
        self._parent = parent
        self._socket = sock
        self._info = info
        self.__running = True
        self._thread = threading.Thread(target=self.__threadLoop)
        self._thread.setDaemon(True)
        self._thread.start()

    def __threadLoop(self):
        self.__logger.info("Starting up Receiver Thread")
        try:
            while self.__running:
                # This call is blocking
                receivedData = self._socket.recv(self.__bufferSize)
                if len(receivedData) == 0:
                    self.__logger.warning("Received empty message. Ignoring")
                    continue
                else:
                    self.__logger.debug("Received data: " + str(receivedData))
                    # debug remove later
                    self._socket.send(receivedData)
                    self._parent.observer.invoke(self, receivedData)
        except BluetoothError as btErr:
            if btErr.errno == 104:
                self.__logger.info("Connection closed by client")
            else:
                self.__logger.warning("Connection closed")
        self.shutdown()
        self._parent.notifyClientStopped(self)

    def send(self, binaryData: bytes):
        self._socket.send(binaryData)

    def shutdown(self):
        self.__logger.info("Shutting down Client " + str(self._info))
        self.__running = False
        self._socket.close()
