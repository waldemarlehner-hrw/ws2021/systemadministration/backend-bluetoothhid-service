# This file takes code from the pybluez sample code:
# https://github.com/pybluez/pybluez/blob/master/examples/simple/rfcomm-server.py
import logging
import subprocess
import time
from threading import Thread
from socket import socket

import coloredlogs
from bluetooth import RFCOMM, advertise_service
from bluetooth.bluez import BluetoothSocket
from bluetooth.btcommon import PORT_ANY, SERIAL_PORT_PROFILE, SERIAL_PORT_CLASS, BluetoothError

from lib.ledHandler import LedHandler
from lib.observer import Observer

_RFCOMM_SERVICE_ID: str = "b551a4c4-4c8c-11ec-81d3-0242ac130003"
"""
This is a "well-known" ID. We chose this UUID at random. A device may only establish a connection 
"""


class BluetoothServer:
    """
    Server that communicates via the RFCOMM Protocol.
    Only one Client is allowed at a time.
    """
    def __init__(self, bufferSize: int):
        self.__logger = logging.getLogger(self.__class__.__name__)
        coloredlogs.install(level="DEBUG", logger=self.__logger)
        self.__logger.info("Creating Bluetooth Server")
        # Make sure HCI Config is set to Pi Scan, Auth is forced, and Encrypt is set
        subprocess.run(["sudo", "hciconfig", "hci0", "piscan", "auth", "encrypt"])
        #
        self.__clients = []
        self.observer = Observer()
        self.bufferSize = bufferSize
        self.socket = BluetoothSocket(RFCOMM)
        self.socket.bind(("", PORT_ANY))
        self.socket: socket
        self.socket.listen(1)
        self.port = self.socket.getsockname()[1]
        self._startRfcommAdvertising()
        self._ledHandler = LedHandler()
        self._running = False

    def start(self):
        """
        Go into "productive" mode.
        Beware that this is blocking!
        """
        self.__logger.info("Creating Main ")
        self._running = True
        while self._running:
            try:
                self.__logger.info("Awaiting new connection...")
                clientSocket, clientInfo = self.socket.accept()
                self.__logger.info("New Connection established")
                from lib.bluetoothClient import BluetoothClient
                self.__clients.append(BluetoothClient(self, clientSocket, clientInfo, self.bufferSize))
                self._ledHandler.notifyAboutNewState(len(self.__clients) == 0)
            except BluetoothError as btErr:
                if btErr.errno == 4:
                    self.__logger.info("Receiving Socket closed by System Interrupt")
                else:
                    self.__logger.error(btErr)

        self.__logger.info("Server shut down.")

    def _startRfcommAdvertising(self):
        advertise_service(self.socket,
                          "RemotePassword",
                          service_id=_RFCOMM_SERVICE_ID,
                          profiles=[SERIAL_PORT_PROFILE],
                          service_classes=[_RFCOMM_SERVICE_ID, SERIAL_PORT_CLASS]
                          )

    def notifyClientStopped(self, client):
        from lib.bluetoothClient import BluetoothClient
        client: BluetoothClient
        self.__clients.remove(client)
        self.__logger.info(self.__clients)
        self._ledHandler.notifyAboutNewState(len(self.__clients) == 0)

    def kill(self):
        for client in self.__clients:
            from lib.bluetoothClient import BluetoothClient
            client: BluetoothClient
            client.shutdown()
        self.__shutdown()

    def __shutdown(self):
        self.__logger.info("Shutting down Bluetooth Server")
        self._running = False
        self.socket: socket
        self.socket.close()



