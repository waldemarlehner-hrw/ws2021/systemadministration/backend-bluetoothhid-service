class Observer:
    def __init__(self):
        self._subscribers = []

    def clear(self):
        self._subscribers = []

    def subscribe(self, callback):
        self._subscribers.append(callback)

    def unsubscribe(self, callback):
        self._subscribers.remove(callback)

    def invoke(self, sender, value):
        for subscriber in self._subscribers:
            subscriber(sender, value)
