from enum import Enum


class FrameType(Enum):
    GET_PUB_KEY_REQUEST = bytes([0x01])
    GET_PUB_KEY_RESPONSE = bytes([0x11])
    COMMIT_REQUEST = bytes([0x02])
    COMMIT_RESPONSE_SUCCESS = bytes([0x12])
    COMMIT_RESPONSE_BAD_PAYLOAD = bytes([0x20])
    COMMIT_RESPONSE_BAD_CHALLENGE = bytes([0x21])
    GENERIC_ERROR_RESPONSE = bytes([0xFF])


def __lengthField(rawData: bytes):
    return rawData[1] << 8 | rawData[2]


def __handleAsGetPubKeyRequest(rawData: bytes):
    d = dict()
    if __lengthField(rawData) != 0:
        d["valid"] = False
        d["reason"] = "Pub Key Request expects to have a Length of 0"
    else:
        d["valid"] = True
        d["type"] = FrameType.GET_PUB_KEY_REQUEST
    return d


def __handleAsCommit(rawData: bytes):
    d = dict()
    lenField = __lengthField(rawData)
    if lenField < 8:
        d["valid"] = False
        d["reason"] = "Frame needs to be at least 8 Bytes long"
        return d
    asByteArray = bytearray(rawData)
    message = asByteArray[3:-8]
    challenge = asByteArray[-8:]
    d["type"] = FrameType.COMMIT_REQUEST
    d["message"] = message
    d["challenge"] = challenge
    return d


def parseInboundMessage(rawData: bytes):
    frameType = rawData[0]
    d = dict()
    if len(rawData) < 3:
        d["valid"] = False
        d["reason"] = "Frame too short"
        return d

    lengthValue = __lengthField(rawData)
    if lengthValue > len(rawData) - 3:
        d["valid"] = False
        d["reason"] = "Message shorter than Header Length"

    if lengthValue < len(rawData) - 3:
        arr = bytearray(rawData)
        arr = arr[:lengthValue+3]
        rawData = bytes(arr)

    match frameType:
        case FrameType.GET_PUB_KEY_REQUEST:
            return __handleAsGetPubKeyRequest(rawData)
        case FrameType.COMMIT_REQUEST:
            return __handleAsCommit(rawData)

    d["valid"] = False
    d["reason"] = "Unknown Frame Type"
    return d
