from Crypto.Cipher import PKCS1_OAEP
from Crypto.PublicKey import RSA
from Crypto.PublicKey.RSA import RsaKey
# https://wizardforcel.gitbooks.io/practical-cryptography-for-developers-book/content/asymmetric-key-ciphers/rsa-encrypt-decrypt-examples.html


class Encryption:

    def __init__(self, keySize: int):
        self.keySize = keySize
        self.key: RsaKey

    def createNewKeypair(self):
        # noinspection PyAttributeOutsideInit
        self.key = RSA.generate(self.keySize)

    def decryptMessage(self, message: bytes):
        decrypter = PKCS1_OAEP.new(self.key)
        decryptedMessage = decrypter.decrypt(message)
        return decryptedMessage.decode()

    def getPublicKey(self):
        pubKey = self.key.public_key()
        return pubKey.export_key(format="DER")

