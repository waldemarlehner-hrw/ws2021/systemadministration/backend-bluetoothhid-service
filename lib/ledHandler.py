import logging
import subprocess
import threading
import time

import coloredlogs


class LedHandler:

    def __init__(self):
        self.__logger = logging.getLogger(self.__class__.__name__)
        coloredlogs.install(level="DEBUG", logger=self.__logger)
        self.__logger.info("Starting LED Handler")
        self.isLedBlinking = False
        subprocess.run(["sudo", "modprobe", "ledtrig_timer"], stdout=subprocess.DEVNULL)
        subprocess.run(["echo \"timer\" | sudo tee /sys/class/leds/led0/trigger > /dev/null"], shell=True,
                       stdout=subprocess.DEVNULL)
        self.__setNewHighDuration(200)
        self.notifyAboutNewState(True)

    def notifyAboutNewState(self, isReadyForNewConnection: bool):
        # Conn Ready = Blinking
        if isReadyForNewConnection == self.isLedBlinking:
            return  # State already present
        self.isLedBlinking = isReadyForNewConnection

        self.__logger.info("Changed LED Blinking State to: "+str(self.isLedBlinking))

        duration = 200 if self.isLedBlinking else 0
        self.__setNewLowDuration(duration)

    def __setNewHighDuration(self, millis: int):
        self.__setNewDuration(millis, "delay_on")

    def __setNewLowDuration(self, millis: int):
        self.__setNewDuration(millis, "delay_off")

    @staticmethod
    def __setNewDuration(millis: int, fileName: str):
        subprocess.run("echo \"" + str(millis) + "\" | sudo tee /sys/class/leds/led0/" + fileName + " > /dev/null",
                       shell=True, stdout=subprocess.DEVNULL)

