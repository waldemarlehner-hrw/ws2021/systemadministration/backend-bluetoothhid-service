import os

from Crypto.Cipher import PKCS1_OAEP
from Crypto.PublicKey.RSA import RsaKey


class ChallengeHandling:
    def __init__(self, lengthInBytes: int, clientPubKeys: list[RsaKey]):
        self._challengeLen = lengthInBytes
        self._pubKeys = clientPubKeys

    def createNewChallenge(self):
        return os.urandom(self._challengeLen)

    def verifyChallenge(self, challenge: bytes, signedChallenge: bytes):
        for key in self._pubKeys:
            encryptor = PKCS1_OAEP.new(key)
            encryptedBytes = encryptor.encrypt(signedChallenge)

            if encryptedBytes == challenge:
                return True
        return False
