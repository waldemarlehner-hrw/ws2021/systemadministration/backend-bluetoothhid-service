from lib.encryption import Encryption
from lib.frame import parseInboundMessage, FrameType


class ResponseHandler:
    def __init__(self, encryptionModule: Encryption):
        self._encryptionModule = encryptionModule

    def handleInboundMessage(self, client, message: bytes):
        from lib.bluetoothClient import BluetoothClient
        client: BluetoothClient
        parsedMessage = parseInboundMessage(message)

        if not parsedMessage["valid"]:
            self.__respondWithGenericError(client, parsedMessage["reason"])
            return


    @staticmethod
    def __respondWithGenericError(client, message: str):
        from lib.bluetoothClient import BluetoothClient
        client: BluetoothClient

        messageAsBytes = bytes(message)
        messageAsBytesLen = len(messageAsBytes)
        frameType: bytes = FrameType.GENERIC_ERROR_RESPONSE.value
        lenByte0 = messageAsBytesLen & 0xFF00 >> 8
        lenByte1 = messageAsBytesLen & 0xFF

        entirePayload = frameType + bytes([lenByte0, lenByte1]) + messageAsBytes
        client.send(entirePayload)
