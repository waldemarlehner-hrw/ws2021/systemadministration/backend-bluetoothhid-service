import logging
import os
import subprocess
from pathlib import Path

import coloredlogs

# Setup Logging
logger = logging.getLogger(__name__)
coloredlogs.install(level="DEBUG", logger=logger)


def handleInboundCleartext(sender, data: bytes):
    try:
        __sendStringToKeyboard(data.decode("utf-8"))
    except Exception as err:
        logger.error(str(err))


def __sendStringToKeyboard(data: str):
    scanPath = (Path(__file__).parent / "../scan").resolve()
    subprocess.run("echo \""+data+"\" | sudo \""+str(scanPath)+"\" /dev/hidg0 2 0", shell=True)


